/* global module */

module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        // Metadata
        pkg: grunt.file.readJSON('package.json'),


        // -- Tasks
        uglify: {
            minified: {
                options: {
                    banner: '/* <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>' +
                    '* by <%= pkg.author %> (http://monkeymonk.be)' +
                    '* forked from Sean McCambridge (http://www.seanmccambridge.com/tubular)' + '* licensed under the MIT License '+
                    '*/'
                },
                files: {
                    'dist/js/jquery-tubular.min.js': [
                        'src/js/jquery.tubular.js'
                    ]
                }
            }, // minified
        } // uglify
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');


    grunt.event.on('watch', function(action, filepath, target) {
        grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
    });


    // -- Tasks

    grunt.registerTask('default', ['uglify']);
};